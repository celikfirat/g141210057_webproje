﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EHaliSaha.Areas.Admin.Controllers
{
    [Authorize(Roles = "yonetici")]
    public class KullanicilarController : Controller
    {   
        EHalisahaEntities2 db = new EHalisahaEntities2();
        // GET: Admin/Kullanicilar  
        public ActionResult Index()
        {
            var kullanicilar = db.kullanicilar.OrderBy(x=>x.id).ToList();
            return View(kullanicilar);
        }    
        public ActionResult Add()
        {
            return View();
        }
        public ActionResult AddKullanici(kullanicilar model)
        {
            if (ModelState.IsValid)
            {
                db.kullanicilar.Add(model);
                db.SaveChanges();
            }
            return RedirectToAction("Index");    
        }
        [HttpGet]
        public ActionResult Edit(int id)
        {
            var model = db.kullanicilar.Find(id);
            //var model = db.kullanicilar.FirstOrDefault(x => x.id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Edit(kullanicilar model)
        {
            if (ModelState.IsValid)
            {
                db.kullanicilar.Attach(model);
                db.Entry(model).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                return View(model);
            }
            return RedirectToAction("Index");
        }
        public ActionResult Delete(int id)
        {
            var model=db.kullanicilar.Find(id);
            return View(model);
        }
        public ActionResult DeleteConfirm(int id)
        {
            var model = db.kullanicilar.Find(id);
            db.kullanicilar.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}