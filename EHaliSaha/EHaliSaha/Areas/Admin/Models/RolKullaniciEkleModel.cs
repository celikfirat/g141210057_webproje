﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EHaliSaha.Areas.Admin.Models
{
    public class RolKullaniciEkleModel
    {
        public string KullaniciAdi { get; set; }
        public string RolAdi { get; set; }
    }
}