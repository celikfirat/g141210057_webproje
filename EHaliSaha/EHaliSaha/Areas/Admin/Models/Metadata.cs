﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace EHaliSaha
{
    public class KullaniciMetadata
    {
        [Required]
        [Display(Name ="Ad Giriniz")]
        public string ad { get; set; }
        [Required]
        [Display(Name = "Soyad Giriniz")]
        public string soyad { get; set; }
        [Required]
        [Display(Name = "Yaş Giriniz")]
        public int yas { get; set; }
        [Required]
        [Display(Name = "Telefon Numarası Giriniz")]
        public string tel { get; set; }
        [Required]
        [Display(Name = "Halı Saha Seçiniz")]
        public string halisaha_adi { get; set; }
        [Required]
        [Display(Name = "Randevu Tarihi Seçiniz")]
        public string randevu_tarihi { get; set; }
    }
}