﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(EHaliSaha.Startup))]
namespace EHaliSaha
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
